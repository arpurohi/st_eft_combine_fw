combineTool.py -M Impacts -n .Impact_ForDDQCD_14Mar_asimov -d datacard_for_Combine_ForDDQCD_14Mar.root --expectSignal 1 -t -1 -m 125 --doInitialFit --robustFit 1
combineTool.py -M Impacts -n .Impact_ForDDQCD_14Mar_asimov -d datacard_for_Combine_ForDDQCD_14Mar.root --expectSignal 1 -t -1 -m 125 --robustFit 1 --doFits
combineTool.py -M Impacts -n .Impact_ForDDQCD_14Mar_asimov -d datacard_for_Combine_ForDDQCD_14Mar.root --expectSignal 1 -t -1 -m 125 -o SingleTop_impacts_2018_asimov_ForDDQCD_14Mar.json
plotImpacts.py -i SingleTop_impacts_2018_asimov_ForDDQCD_14Mar.json -o SingleTop_impacts_2018_asimov_ForDDQCD_14Mar$1
mv SingleTop_impacts_2018_asimov_ForDDQCD_14Mar$1.pdf /home/cms/apurohit/Combine_Plots/
