import os
import shutil
import subprocess

# Function to update the line containing qcd_mu in the data card
def update_datacard(datacard_path, new_qcd_mu):
    with open(datacard_path, 'r') as file:
        lines = file.readlines()

    # Find the line containing qcd_mu and update it
    updated_lines = [line.replace('qcd_mu', new_qcd_mu) if 'qcd_mu' in line else line for line in lines]

    # Write the updated lines back to the data card
    with open(datacard_path, 'w') as file:
        file.writelines(updated_lines)

# List of directory names based on iso cuts
iso_cut_names = ['isoCut-sup15', 'isoCut-sup20', 'isoCut-sup25', 'isoCut-sup30', 'isoCut-sup35', 'isoCut-sup40', 'isoCut-sup45']

# Path to the original data card file
original_datacard_path = 'datacard_for_Combine_ForDDQCD_14Mar.txt'

# Read the data card to find the line containing qcd_mu
with open(original_datacard_path, 'r') as file:
    lines = file.readlines()
    for line in lines:
        if 'qcd_mu' in line:
            qcd_mu_line = line
            print(qcd_mu_line)
            break

# Create directories, copy data card, and update qcd_mu line in each directory
for iso_cut_name in iso_cut_names:
    # Create directory
    os.makedirs(iso_cut_name, exist_ok=True)
    print(iso_cut_name)    
    # Path to the new data card file in the directory
    new_datacard_path = os.path.join(iso_cut_name, 'datacard_for_Combine_ForDDQCD_14Mar.txt')

    # Copy original data card file to the new directory
    shutil.copy(original_datacard_path, new_datacard_path)
    shutil.copy("produce_impacts_Nicolas.sh", iso_cut_name+"/")
    print("Dir created and files are copied ********")

    # Update qcd_mu line in the new data card file
    #new_qcd_mu = qcd_mu_line.replace('qcd_mu', f'qcd_mu_{iso_cut_name}')
    update_datacard(new_datacard_path, f'qcd_mu_{iso_cut_name}')
    print("Datacard is updated ********")
    # Run command inside the directory
    os.chdir(iso_cut_name)
    command = f"text2workspace.py datacard_for_Combine_ForDDQCD_14Mar.txt --channel-masks -o datacard_for_Combine_ForDDQCD_14Mar.root"
    print(f"Running {command}")
    subprocess.run(command, shell=True)
    print("workspace is produced")
    impact_plot_command = f"./produce_impacts_Nicolas.sh _DataDrivenQCD_{iso_cut_name}"
    print(f"Running {impact_plot_command}")
    subprocess.run(impact_plot_command, shell=True)
    print(f"Finished with {iso_cut_name}")
    os.chdir('..')  # Go back to the original directory


print("Finished !!!!!!")
