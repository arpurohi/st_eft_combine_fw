import subprocess
import sys
import argparse
import os
import CMS_lumi
import tdrstyle
import ROOT
#from ROOT import TCanvas, TPad, gStyle, TH1F
from ROOT import TFile, TH1, TH2, TCanvas, TH1F, THStack, TString
from ROOT import TLegend, TApplication, TRatioPlot, TPad, TFrame
from ROOT import TGraphAsymmErrors
from ROOT import TStyle, gStyle, TColor, TLatex
import array
import math
from style_manager import *

tdrstyle.setTDRStyle()
#ROOT.gROOT.SetStyle("tdrstyle")

def run_command(command, log_file):
    """Run a shell command and save the output to a log file."""
    with open(log_file, "w") as log:
        try:
            output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
            log.write(output.decode())
            print(f"Output saved to {log_file}")
        except subprocess.CalledProcessError as e:
            log.write(f"Command failed with exit code {e.returncode}: {command}\n")
            log.write(e.output.decode())
            print(f"Command failed with exit code {e.returncode}. See {log_file} for details.")
            sys.exit(e.returncode)

def text_to_workspace(datacard):
    """Convert the datacard to a ROOT workspace."""
    output_workspace = datacard.replace('.txt', '.root').replace('datacard', 'workspace')
    log_file = output_workspace.replace('.root', '_text2workspace.log')
    command = f"text2workspace.py {datacard} -o {output_workspace}"
    run_command(command, log_file)
    return output_workspace

def run_fit_diagnostics(workspace, observable, year, asimov):
    """Run the FitDiagnostics command."""
    output_fitdiagnostics = workspace.replace('workspace', '').split('.root')[0]+'.'+observable+'_inclusive_'+year
    log_file = workspace.replace('.root', '_FitDiagnostics.log')
    rbin='r'
    r_range='0.5,1.5'
    npoints=20
    asi = ''
    sasimov=''
    if asimov == 'asimov':
        print('################')
        print('# Asimov test : ')
        print('################')
        print('')
        asi = '--expectSignal 1 -t -1'
        sasimov = '_asimov'
    else:
        sasimov = '_data'

    if (asimov=='asimov'):
        finput = workspace
    else:
        cmd2 = 'combine -M MultiDimFit -n .snapshot_'+year+'_'+asimov+' --robustFit 1 '
        cmd2 += ' -d '+workspace
        cmd2 += asi #asimov_param(asimov)
        cmd2 += ' --parameters '+rbin+' --setParameterRanges '+rbin+'='+r_range #+' --floatOtherPOIs=1'
        cmd2 += ' --saveWorkspace'
        os.system('echo '+cmd2)
        os.system(cmd2)
        finput = "higgsCombine.snapshot_"+year+"_"+asimov+".MultiDimFit.mH120.root --snapshotName MultiDimFit"

    cmd5 = 'combineTool.py -M FitDiagnostics '+finput+' -m 125 --rMin 0 --rMax 10 --cminDefaultMinimizerStrategy 0 --saveShapes --saveWithUncertainties '
    #cmd5 += asi
    cmd5 += ' --skipBOnlyFit --plots'
    cmd5 += ' -n '+output_fitdiagnostics
    os.system('echo using the root file : '+workspace)
    os.system('echo '+cmd5)
    #os.system(cmd5)


    #command = f"combine -M FitDiagnostics {workspace} --saveShapes --saveWithUncertainties --plots -n {output_fitdiagnostics}"
    run_command(cmd5, log_file)
    return 'fitDiagnostics'+output_fitdiagnostics


def generate_correlation_plot(fit_diagnostics_file, observable, year, channel, region, outputpath, extension):
    """Generate a correlation matrix plot from FitDiagnostics results."""
    gStyle.SetPalette(55)
    gStyle.SetOptStat(0)

    canvas = TCanvas('CorrelationMatrix', 'CorrelationMatrix', 1000, 800)
    pad = TPad("pad", "pad", 0, 0, 1, 1)
    pad.SetLeftMargin(0.16)
    pad.SetBottomMargin(0.2)
    pad.SetRightMargin(0.1)
    pad.Draw()
    pad.cd()

    fDiagnostics = ROOT.TFile.Open(fit_diagnostics_file, "READ")
    hCov = fDiagnostics.Get("covariance_fit_s")
    hCov.GetXaxis().LabelsOption("v")
    hCov.GetXaxis().SetLabelSize(0.025)
    hCov.GetYaxis().SetLabelSize(0.025)
    hCov.GetZaxis().SetLabelSize(0.025)
    hCov.SetTitle(f"Systematics correlation matrix, Channel: {channel}, Region:{region}, {year}")
    palette = hCov.GetListOfFunctions().FindObject("palette")
    palette.SetX1NDC(0.92)
    palette.SetX2NDC(0.94)
    palette.SetY1NDC(0.2)
    palette.SetY2NDC(0.9)
    hCov.Draw("COLZ")
    output_filename = f"{outputpath}/CorrelationMatrixParameters_{observable}_{channel}_{region}_{year}_{extension}.pdf"
    canvas.Print(output_filename)
    print(f"Correlation matrix plot saved to {output_filename}")


def getHistWithXaxis(hist, mymin=0, mymax=0):
    nbin = hist.GetNbinsX()
    if mymin==0:
        xmin = hist.GetXaxis().GetXmin()
    else:
        xmin = float(mymin)
    if mymax==0:
        xmax = hist.GetXaxis().GetXmax()
    else:
        xmax = float(mymax)
    hist_new = TH1F(hist.GetName()+"_new", hist.GetName()+"_new", nbin, xmin, xmax)
    for i in range(nbin):
        hist_new.SetBinContent(i+1,hist.GetBinContent(i+1))
        hist_new.SetBinError(i+1,hist.GetBinError(i+1))
    return hist_new

def getGraphWithXaxis(graph, nbin, min_bin, width_bin):
    x = []
    ex_left = []
    ex_right =  []
    y = []
    ey_up  = []
    ey_down = []
    for i in range(nbin):
        x.append(min_bin+width_bin/2.+width_bin*i)
        ex_left.append(width_bin/2.)
        ex_right.append(width_bin/2.)
        y.append(graph.GetY()[i])
        ey_up.append(graph.GetEYhigh()[i])
        ey_down.append(graph.GetEYlow()[i])
    graph_new = TGraphAsymmErrors(len(x),array.array('d', x),array.array('d', y),array.array('d', ex_left),array.array('d', ex_right),array.array('d', ey_down),array.array('d', ey_up))
    graph_new.SetName(graph.GetName()+"_new")
    graph_new.SetTitle(graph.GetName()+"_new")
    return graph_new

def getUncertaintyBandGraph(hist):
    x = []
    ex_left = []
    ex_right =  []
    y =  []
    ey_up  = []
    ey_down = []
    nbin = hist.GetNbinsX()
    min_bin = hist.GetXaxis().GetXmin()
    max_bin = hist.GetXaxis().GetXmax()
    width_bin = (max_bin-min_bin)/nbin

    for i in range(nbin):
        x.append(min_bin+width_bin/2.+width_bin*i)
        ex_left.append(width_bin/2.)
        ex_right.append(width_bin/2.)
        y.append(hist.GetBinContent(i+1))
        ey_up.append(hist.GetBinErrorUp(i+1))
        ey_down.append(hist.GetBinErrorLow(i+1))
    graph_new = TGraphAsymmErrors(len(x),array.array('d', x),array.array('d', y),array.array('d', ex_left),array.array('d', ex_right),array.array('d', ey_down),array.array('d', ey_up))
    graph_new.SetName("uncertainty_band_"+hist.GetName())
    graph_new.SetTitle("uncertainty_band_"+hist.GetName())
    return graph_new

def getUncertaintyBandRatioGraph(hist):
    x = []
    ex_left = []
    ex_right =  []
    y =  []
    ey_up  = []
    ey_down = []
    nbin = hist.GetNbinsX()
    min_bin = hist.GetXaxis().GetXmin()
    max_bin = hist.GetXaxis().GetXmax()
    width_bin = (max_bin-min_bin)/nbin
    for i in range(nbin):
        x.append(min_bin+width_bin/2.+width_bin*i)
        ex_left.append(width_bin/2.)
        ex_right.append(width_bin/2.)
        y.append(1)
        ey_up.append(hist.GetBinErrorUp(i+1)/hist.GetBinContent(i+1))
        ey_down.append(hist.GetBinErrorLow(i+1)/hist.GetBinContent(i+1))
    graph_new = TGraphAsymmErrors(len(x),array.array('d', x),array.array('d', y),array.array('d', ex_left),array.array('d', ex_right),array.array('d', ey_down),array.array('d', ey_up))
    graph_new.SetName("uncertainty_band_ratio_"+hist.GetName())
    graph_new.SetTitle("uncertainty_band_ratio_"+hist.GetName())
    return graph_new

def displayPrePostFitPlot(fit_diagnostics_file, fitkind, title, region, channel, my_xmin, my_xmax, doLog, outpath, extension):
    
    r = 0.3
    epsilon = 0.1
    fDiagnostics = ROOT.TFile.Open(fit_diagnostics_file, "READ")
    canvas = TCanvas('stack_'+region+fitkind,'stack_'+region+fitkind, 800, 800)
    canvas.UseCurrentStyle()
    
    if (fitkind=='prefit'):
        sfitkind = "Pre-fit"
    else:
        sfitkind = "Post-fit"
        
    pad1 = TPad("pad1", "pad1", 0, r-epsilon, 1, 1)
    pad1.SetBottomMargin(epsilon)
    
    tm = gStyle.GetPadTopMargin()
    print('TopMargin: '+str(tm)+' -> '+str(1.5*tm))
    gStyle.SetPadTopMargin(1.5*tm) #Was 1.5
    pad1.SetTopMargin(1.5*tm) #Was 1.5
    
    canvas.cd()
    if (doLog): pad1.SetLogy()
    pad1.Draw()
    pad1.cd()
    
    print("shapes_"+fitkind+"/"+region+"/total")

    hist_total = getHistWithXaxis(fDiagnostics.Get("shapes_"+fitkind+"/"+region+"/total"), my_xmin, my_xmax)
    nbin = hist_total.GetNbinsX()
    min_bin = hist_total.GetXaxis().GetXmin()
    max_bin = hist_total.GetXaxis().GetXmax()
    width_bin = (max_bin-min_bin)/nbin
    hist_data = getGraphWithXaxis(fDiagnostics.Get("shapes_"+fitkind+"/"+region+"/data"), nbin, min_bin, width_bin)
    hist_signal = getHistWithXaxis(fDiagnostics.Get("shapes_"+fitkind+"/"+region+"/signal"), my_xmin, my_xmax)
    hist_singletop = getHistWithXaxis(fDiagnostics.Get("shapes_"+fitkind+"/"+region+"/singletop"), my_xmin, my_xmax)
    hist_vjets = getHistWithXaxis(fDiagnostics.Get("shapes_"+fitkind+"/"+region+"/vjets"), my_xmin, my_xmax)
    hist_ttx = getHistWithXaxis(fDiagnostics.Get("shapes_"+fitkind+"/"+region+"/ttbar"), my_xmin, my_xmax)
    hist_diboson = getHistWithXaxis(fDiagnostics.Get("shapes_"+fitkind+"/"+region+"/diboson"), my_xmin, my_xmax)
    hist_qcd = getHistWithXaxis(fDiagnostics.Get("shapes_"+fitkind+"/"+region+"/qcd_mu_dd"), my_xmin, my_xmax)
    #hist_drellyan = getHistWithXaxis(fDiagnostics.Get("shapes_"+fitkind+"/"+region+"/drellyan"))
    UncertaintyBand = getUncertaintyBandGraph(hist_total)
    UncertaintyBandRatio = getUncertaintyBandRatioGraph(hist_total)
    
    if sfitkind=="Pre-fit":
        sfitkind_corrected = "Prefit"
    else:
        sfitkind_corrected = "Postfit"
    ##legend_args = (0.645, 0.56, 0.85, 0.9, sfitkind_corrected, 'NDC')
    legend_args = (0.615, 0.56, 0.83, 0.9, sfitkind_corrected, 'NDC')
    #legend_args = (0.645, 0.65, 0.85, 0.92, sfitkind_corrected, 'NDC')
    legend = TLegend(*legend_args)
    legend.SetTextSize(0.05)
    legend.SetBorderSize(0)
    legend.AddEntry(hist_data, "Data","ep")
    legend.AddEntry(hist_signal, "t-channel SM", "f")
    legend.AddEntry(hist_singletop, "s-channel/tW", "f")
    legend.AddEntry(hist_ttx, "t#bar{t}", "f")
    #legend.AddEntry(hist_background, "non-t#bar{t}", "f")
    legend.AddEntry(hist_diboson, "Diboson", "f")
    legend.AddEntry(hist_vjets, "W/Z+jets", "f")
    legend.AddEntry(hist_qcd, "QCD data driven", "f")
    #legend.AddEntry(hist_drellyan, "Z+Jets", "f")



    stack = THStack()
    stack.Add(hist_qcd)
    stack.Add(hist_vjets)
    stack.Add(hist_diboson)
    stack.Add(hist_ttx)
    stack.Add(hist_singletop)
    #stack.Add(hist_drellyan)
    stack.Add(hist_signal)
    if (doLog): stack.SetMinimum(10)
    
    UncertaintyBand.GetXaxis().SetLimits(min_bin,max_bin)
    #UncertaintyBand.GetXaxis().SetRangeUser(min_bin,max_bin)
    UncertaintyBand.SetMinimum(0)
    if (doLog): UncertaintyBand.SetMinimum(10)
    
    stack.Draw()
    UncertaintyBand.GetYaxis().SetTitleOffset(0.95)
    UncertaintyBand.Draw("2AP SAME")
    stack.Draw("HIST SAME")
    hist_data.Draw("PSAME")
    legend.Draw("SAME")

    style_histo(hist_signal, ROOT.TColor.GetColor('#BF0000'), 1, ROOT.TColor.GetColor('#BF0000'), 1001, 0) #3004
    style_histo(hist_singletop, ROOT.TColor.GetColor('#FFBF00'), 1, ROOT.TColor.GetColor('#FFBF00'), 1001, 0) #3005
    style_histo(hist_ttx, ROOT.TColor.GetColor('#ff8c00'), 1, ROOT.TColor.GetColor('#ff8c00'), 1001, 0) #3005
    style_histo(hist_diboson, ROOT.TColor.GetColor('#0000FF'), 1, ROOT.TColor.GetColor('#0000FF'), 1001, 0) #3005
    style_histo(hist_vjets, ROOT.TColor.GetColor('#339933'), 1, ROOT.TColor.GetColor('#339933'), 1001, 0) #3005
    #style_histo(hist_drellyan, 619, 1, 619, 3002, 0) #3005
    style_histo(hist_qcd, ROOT.TColor.GetColor('#cccccc'), 1, ROOT.TColor.GetColor('#cccccc'), 1001, 0) #3005
    style_histo(hist_data, 1, 1, 0, 3001, 1, 20)


    style_histo(UncertaintyBand, 1, 1, 1, 3005, 0)
    style_labels_counting(UncertaintyBand, 'Events', title)
    UncertaintyBand.GetXaxis().SetLabelSize(0)
    UncertaintyBand.GetXaxis().SetTitleSize(0)

    
    if(year=='2016'):
        tdrstyle.cmsPrel(35900., 13.,simOnly=False,thisIsPrelim=True)
    elif(year=='2017'):
        tdrstyle.cmsPrel(41500., 13.,simOnly=False,thisIsPrelim=True)
    elif(year=='2018'):
        tdrstyle.cmsPrel(59700., 13.,simOnly=False,thisIsPrelim=True)
    

    pad2 = TPad("pad2", "pad2", 0, 0, 1, r*(1-epsilon))
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.4)
    pad2.SetFillStyle(0)
    canvas.cd()
    pad2.Draw()
    pad2.cd()

    ratio_coef = 0.3

    h_one = TH1F("one", "one", 1, min_bin, max_bin)
    h_one.SetBinContent(1, 1)
    h_one.SetLineWidth(1)
    h_one.SetLineColor(15)
    #h_num = hist_data.Clone()
    h_denom = hist_total
    #h_num.Divide(h_denom)
    h_num = hist_total.Clone()
    for i in range(nbin):
        h_num.SetBinContent(i+1,hist_data.GetY()[i]/hist_total.GetBinContent(i+1))
        h_num.SetBinError(i+1,hist_data.GetEYhigh()[i]/hist_total.GetBinContent(i+1))
    h_num.GetXaxis().SetTitle("aksjd")
    ratio = THStack()
    ratio.Add(h_num)
    
    UncertaintyBandRatio.GetXaxis().SetLimits(min_bin,max_bin)
    UncertaintyBandRatio.Draw("2A SAME")
    h_num.Draw("E SAME")
    h_one.Draw("SAME")

    style_histo(UncertaintyBandRatio, 1, 1, 1, 3005, 0)
    UncertaintyBandRatio.GetXaxis().SetRangeUser(min_bin,max_bin)
    UncertaintyBandRatio.SetMinimum(1-ratio_coef)
    UncertaintyBandRatio.SetMaximum(1+ratio_coef-0.01)

    style_labels_counting(UncertaintyBandRatio, 'Data/MC', title)
    UncertaintyBandRatio.GetYaxis().SetLabelSize(0.13)
    UncertaintyBandRatio.GetYaxis().SetTitleSize(0.15)
    UncertaintyBandRatio.GetYaxis().SetTitleOffset(0.4)
    UncertaintyBandRatio.GetXaxis().SetLabelSize(0.15)
    UncertaintyBandRatio.GetXaxis().SetTitleSize(0.17)
    UncertaintyBandRatio.GetXaxis().SetLabelOffset(0.01)

    resultname = outpath+'/'+year+'/'+observable+'_'+channel+region+year+'_'+sfitkind+extension
    canvas.SaveAs(resultname+'.pdf')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process and analyze datacard and produce the Prefit and postfit plots.')
    parser.add_argument('datacard', help='Path to the datacard')
    parser.add_argument('--doWorkspace', action='store_true', help='Generate Workspace')
    parser.add_argument('--doFit', action='store_true', help='Run fitdiagnostics')
    parser.add_argument('--observable', help='Display your observable', required=True)
    parser.add_argument('--my_xmin', help='X Min', default=0)
    parser.add_argument('--my_xmax', help='X Max', default=0)
    parser.add_argument('--channel', help='channel', default='mu')
    parser.add_argument('--region', help='region', default='WJets_CR')
    parser.add_argument('--year', help='Year of samples', required=True)
    parser.add_argument('--asimov', help='Set if asimov test', default='', nargs='?')
    parser.add_argument('--title', help='Display your observable title', required=True)
    parser.add_argument('--doCorrPlot', action='store_true', help='Generate correlation plot')
    parser.add_argument('--doLog', action='store_true', help='Generate Log Y plot')
    parser.add_argument('--outpath', help='Output Forectory', default='')
    parser.add_argument('--ext', help='Extra Name', default='')
    args = parser.parse_args()

    print(f"Processing datacard: {args.datacard} for {args.observable} ({args.year})")
    
    datacard = args.datacard
    observable = args.observable
    year = args.year
    channel = args.channel
    region = args.region
    outpath = args.outpath
    # Step 1: Convert datacard to workspace
    if(args.doWorkspace):
        workspace = text_to_workspace(datacard)
        print(f"Created workspace: {workspace}")
    else:
        workspace = datacard.replace('.txt', '.root').replace('datacard', 'workspace')




    # Step 2: Run FitDiagnostics
    # Uncomment the next two lines to run FitDiagnostics and save its log
    if(args.doFit):
        fit_diagnostics_output = run_fit_diagnostics(workspace, observable, year, args.asimov)
        fit_diagnostics_output += '.root'
        print(f"Completed FitDiagnostics: {fit_diagnostics_output}")
    else:
        fit_diagnostics_output = workspace.split('.root')[0].replace('workspace', 'fitDiagnostics')+'.'+observable+'_inclusive_'+year+'.root'


    # Step 3: Plotting
    if(args.doCorrPlot):
        generate_correlation_plot(fit_diagnostics_output, observable, year, channel, region, outpath, args.ext)
    
    if args.asimov=="asimov":
        displayPrePostFitPlot(fit_diagnostics_output, "prefit", args.title, region, channel, args.my_xmin, args.my_xmax, args.doLog, outpath, args.ext)
    if args.asimov=="data":
        displayPrePostFitPlot(fit_diagnostics_output, "fit_s", args.title, region, channel, args.my_xmin, args.my_xmax, args.doLog,  outpath, args.ext)
