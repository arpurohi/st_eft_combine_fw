import ROOT
import shutil

def scale_histogram(histogram, scaling_factor):
    """Scale the histogram by the given scaling factor."""
    histogram.Scale(scaling_factor)

def add_histograms(main_combine_file, input_files, output_file):
    """Add histograms from the input file to the output file."""
    #input_root_file = ROOT.TFile.Open(main_combine_file)
    shutil.copy(main_combine_file, output_file)
    output_root_file = ROOT.TFile.Open(output_file, "UPDATE")
    # Get the QCD histogram from the input file
    qcd_histogram = output_root_file.Get("qcd_mu")
    if not qcd_histogram:
        print("Error: QCD histogram not found in input file.")
        return

    # Get the normalization of QCD histogram
    qcd_normalization = qcd_histogram.Integral()

    # Loop over the input files and scale and add the histograms
    for input_file_path in input_files:
        # Extract the suffix from the input file name (e.g., "_sup25")
        suffix = input_file_path.split("_")[-2].replace(".root", "")

        # Open the input file and get the Wboson_transversMass_cut000 histogram
        input_root_file = ROOT.TFile.Open(input_file_path)
        if not input_root_file:
            print(f"Error: Unable to open input file '{input_file_path}'")
            continue

        # Get the Wboson_transversMass_cut000 histogram
        wboson_histogram = input_root_file.Get("Wboson_transversMass_cut000")
        if not wboson_histogram:
            print(f"Error: Wboson_transversMass_cut000 histogram not found in file '{input_file_path}'")
            input_root_file.Close()
            continue

        # Scale the Wboson histogram to the same normalization as QCD
        wboson_normalization = wboson_histogram.Integral()
        scaling_factor = qcd_normalization / wboson_normalization
        scaled_histogram = wboson_histogram.Clone(f"qcd_mu_{suffix}")
        scale_histogram(scaled_histogram, scaling_factor)

        # Write the scaled histogram to the output file
        output_root_file.cd()
        scaled_histogram.Write()

        # Close the input file
        input_root_file.Close()

    # Close the output file
    output_root_file.Close()

if __name__ == "__main__":
    # Define the input files and output file
    input_files = """DATA_muChannel_QCDregion_isoCut-sup15_nbTasksall.root DATA_muChannel_QCDregion_isoCut-sup30_nbTasksall.root  DATA_muChannel_QCDregion_isoCut-sup45_nbTasksall.root
                   DATA_muChannel_QCDregion_isoCut-sup20_nbTasksall.root  DATA_muChannel_QCDregion_isoCut-sup35_nbTasksall.root
                   DATA_muChannel_QCDregion_isoCut-sup25_nbTasksall.root  DATA_muChannel_QCDregion_isoCut-sup40_nbTasksall.root""".split()
    output_file = "/eos/lyoeos.in2p3.fr/grid/cms/store/user/apurohit/SingleTop_Analysis/results/18/2024-03-11/muChannel_With_MediumBTag_nbTasksall/normalized/Wboson_transversMass_rootfile_forCombine_withQCD_DD.root"
    main_combine_file = "/eos/lyoeos.in2p3.fr/grid/cms/store/user/apurohit/SingleTop_Analysis/results/18/2024-03-11/muChannel_With_MediumBTag_nbTasksall/normalized/Wboson_transversMass_rootfile_forCombine.root"
    # Call the function to add histograms
    add_histograms(main_combine_file, input_files, output_file)
