combineTool.py -M Impacts -n .Impact_ForDDQCDVJetsSignal_25Mar_asimov -d workspace_Wboson_transversMass_WJets_CR_VJetsSignal.root --expectSignal 1 -t -1 -m 125 --doInitialFit --robustFit 1
combineTool.py -M Impacts -n .Impact_ForDDQCDVJetsSignal_25Mar_asimov -d workspace_Wboson_transversMass_WJets_CR_VJetsSignal.root --expectSignal 1 -t -1 -m 125 --robustFit 1 --doFits
combineTool.py -M Impacts -n .Impact_ForDDQCDVJetsSignal_25Mar_asimov -d workspace_Wboson_transversMass_WJets_CR_VJetsSignal.root --expectSignal 1 -t -1 -m 125 -o SingleTop_impacts_2018_asimov_ForDDQCDVJetsSignal_25Mar.json
plotImpacts.py -i SingleTop_impacts_2018_asimov_ForDDQCDVJetsSignal_25Mar.json -o SingleTop_impacts_2018_asimov_ForDDQCD_VJetsSignal_25Mar$1
mv SingleTop_impacts_2018_asimov_ForDDQCD_VJetsSignal_25Mar$1.pdf /home/cms/apurohit/Combine_Plots/
