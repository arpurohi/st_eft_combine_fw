imax 1 number of bins
jmax * number of background processes
kmax * number of nuisance parameters
---------------------------------------------------------------------------------------------
shapes signal * ./phi_star_new_rootfile_forCombine.root $PROCESS $PROCESS_phi_star_new_$SYSTEMATIC
shapes singletop * ./phi_star_new_rootfile_forCombine.root $PROCESS $PROCESS_phi_star_new_$SYSTEMATIC
shapes ttbar * ./phi_star_new_rootfile_forCombine.root $PROCESS $PROCESS_phi_star_new_$SYSTEMATIC
shapes vjets * ./phi_star_new_rootfile_forCombine.root $PROCESS $PROCESS_phi_star_new_$SYSTEMATIC
shapes diboson * ./phi_star_new_rootfile_forCombine.root $PROCESS $PROCESS_phi_star_new_$SYSTEMATIC
shapes qcd_mu_dd * ./phi_star_new_rootfile_forCombine.root $PROCESS $PROCESS_$SYSTEMATIC
shapes data_obs * ./phi_star_new_rootfile_forCombine.root $PROCESS
---------------------------------------------------------------------------------------------
bin             SingleTop_SR   
observation     1623944.0      
---------------------------------------------------------------------------------------------
bin                                                     SingleTop_SR    SingleTop_SR    SingleTop_SR    SingleTop_SR    SingleTop_SR    SingleTop_SR   
process                                                 signal          singletop       ttbar           vjets           diboson         qcd_mu_dd         
process                                                 0               1               2               3               4               5              
rate                                                    -1              -1              -1              -1              -1              -1             
---------------------------------------------------------------------------------------------
rsingletop                     lnN                       -               1.1             -               -               -               -              
rttbar                         lnN                       -               -               1.04            -               -               -              
rvjets                         lnN                       -               -               -               1.1             -               -              
rdiboson                       lnN                       -               -               -               -               1.25            -              
rqcd_mu_dd                     lnN                       -               -               -               -               -               1.2            
syst_muon_hlt                  shape                     1               1               1               1               1               -              
syst_muon_reco                 shape                     1               1               1               1               1               -              
syst_muon_id                   shape                     1               1               1               1               1               -              
syst_muon_iso                  shape                     1               1               1               1               1               -              
syst_pu                        shape                     1               1               1               1               1               -              
syst_b_correlated              shape                     1               1               1               1               1               -              
syst_b_uncorrelated_2018       shape                     1               1               1               1               1               -              
syst_l_correlated              shape                     1               1               1               1               1               -              
syst_l_uncorrelated_2018       shape                     1               1               1               1               1               -              
syst_prefiring                 shape                     1               1               1               1               1               -              
syst_medID_                    shape                     -               -               -               -               -               1            
syst_MCsub_                    shape                     -               -               -               -               -               1            
lumi_uncor_2018                lnN                       1.015           1.015           1.015           1.015           1.015           1.015          
lumi_cor_2016_2018             lnN                       1.02            1.02            1.02            1.02            1.02            1.02           
lumi_cor_2017_2018             lnN                       1.002           1.002           1.002           1.002           1.002           1.002          
--------------------------------------------------------------------------------------------
