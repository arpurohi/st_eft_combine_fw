# ST_EFT_Combine_FW



## Getting started

We need to create a CMSSW area,
cmsrel CMSSW_11_3_4

Then we need to install Higgs combine 

```
cd CMSSW_11_3_4/src
cmsenv
git clone https://github.com/cms-analysis/HiggsAnalysis-CombinedLimit.git HiggsAnalysis/CombinedLimit

cd $CMSSW_BASE/src/HiggsAnalysis/CombinedLimit
git fetch origin
git checkout v9.2.1
scramv1 b clean; scramv1 b # always make a clean build

```


Now we need to install CombineHarvetser package

```
cd $CMSSW_BASE/src/
# IMPORTANT: Checkout the recommended tag on the link above
git clone https://github.com/cms-analysis/CombineHarvester.git CombineHarvester
cd $CMSSW_BASE/src/CombineHarvester/
git checkout v2.0.0
scram b
```


Example execution of the prefit and postfit script (run_pre_post-fit.py),

```
python3 run_pre_post-fit.py datacard_Wboson_transversMass_WJets_CR_VJetsSignal.txt --doWorkspace --doFit --observable Wboson_TransverseMass --my_xmin 0 --my_xmax 200 --asimov="data" --year 2018 --title 'W boson M_{T} [GeV]' --ext '25Apr' --outpath '/home/cms/apurohit/Combine_Plots/’
```

Example execution of the Impact plot script,

```
./produce_impacts_Nicolas.sh
```